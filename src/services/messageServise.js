export const getMessages = () => {
    return fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then(data => data.json())
        .then(data => {
            return data.map(item => {
                return {...item, isLiked: false}
            })
        })
}
