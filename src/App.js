import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Chat from "./containers/Chat";
import './App.css';

const App = () => {
    return (
        <div className="mainContainer">
            <Header />
            <Chat />
            <Footer />
        </div>
    );
}

export default App;
