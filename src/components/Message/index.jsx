import React from 'react';
import PropTypes from 'prop-types';
import AvatarMessage from "../AvatarMesage";

import styles from "./styles.module.css";

const Message = ({
                     id,
                     user,
                     avatar,
                     text,
                     date,
                     userId,
                     isLiked,
                     authUser,
                     reactMessage,
                     setEditStatusMessage,
                     deleteMessage
                 }) => {

    const isAuthUsersMessage = !(userId === authUser.userId);

    const handleReactMessage = id => {
        reactMessage(id);
    };

    const handleEditMessage = (text, id) => {
        setEditStatusMessage({text, id})
    };

    const handleDeleteMessage = id => {
        deleteMessage(id)
    };

    return (
        isAuthUsersMessage
            ? (
                <div className={styles.container}>
                    <div className={isAuthUsersMessage ? styles.messageContainer : styles.myMessageContainer}>
                        <AvatarMessage src={avatar}/>
                        <div className={isAuthUsersMessage ? styles.message : styles.myMessage}>{text}</div>
                        <div className={isLiked ? styles.like : styles.dislike} onClick={() => handleReactMessage(id)}>
                            <i className="fas fa-thumbs-up"></i>
                        </div>
                        <div className={styles.date}>{date}</div>
                    </div>
                </div>
            )
            : (
                <div className={styles.container}>
                    <div className={isAuthUsersMessage ? styles.messageContainer : styles.myMessageContainer}>
                        <div className={isAuthUsersMessage ? styles.date : styles.myDate}>{date}</div>
                        <div className={isAuthUsersMessage ? styles.message : styles.myMessage}>{text}</div>
                        <div className={styles.edit} onClick={() => handleEditMessage(text, id)}>
                            <i className="fas fa-edit"></i>
                        </div>
                        <div className={styles.delete} onClick={() => handleDeleteMessage(id)}>
                            <i className="fas fa-trash-alt"></i>
                        </div>
                        <AvatarMessage src={avatar}/>
                    </div>
                </div>
            )

    );
};

Message.propTypes = {
    id: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userId: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    isLiked: PropTypes.bool,
    authUser: PropTypes.object.isRequired,
    reactMessage: PropTypes.func.isRequired,
    setEditStatusMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired
};

export default Message;
