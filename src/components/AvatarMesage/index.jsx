import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";

const AvatarMessage = ({src}) => {
    return (
        <div>
            <div className={styles.avatarContainer}>
                <img className={styles.imgAvatar} src={src} alt="Logo"/>
            </div>
        </div>
    );
};

AvatarMessage.propTypes = {
    src: PropTypes.string.isRequired
};

export default AvatarMessage;
