import React, {useEffect, useState} from 'react';
import {v4 as uuid} from "uuid";
import ChatFooter from "../../components/ChatFooter";
import ChatHeader from "../../components/ChatHeader";
import MessagesList from "../../components/MessagesList";
import {getMessages} from '../../services/messageServise'
import Spinner from "../../components/Spinner";

import styles from "./styles.module.css";
import EditMessage from "../../components/EditMessage";

const Chat = () => {
    const authUser = {
        user: "Roman Mykytka",
        avatar: "https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg",
        userId: "dfv5v556-1fv5-1111-9999-77777aassddf",
        isOnline: true
    };
    const [messages, setMessages] = useState([]);
    const [countMessages, setCountMessages] = useState(0);
    const [countUsers, setCountUsers] = useState(0);
    const [dateLastMessage, setDateLastMessage] = useState('');

    const [editedMessage, setEditedMessage] = useState(undefined);

    useEffect(() => {
        getMessages()
            .then(messages => {
                console.log(messages);
                setMessages(messages);
                setCountMessages(messages.length);
                setCountUsers(new Set(messages.map(msg => msg.userId)).size);
                setDateLastMessage(messages[messages.length - 1].createdAt);
            })
    }, []);

    const addMessage = (text, {userId, avatar, user}) => {
        setMessages([
            ...messages,
            {
                id: uuid(),
                userId,
                user,
                avatar,
                text,
                createdAt: new Date().toISOString(),
                editedAt: new Date().toISOString()
            }
        ]);
        setDateLastMessage(new Date().toISOString());
    };


    const setEditStatusMessage = message => {
        setEditedMessage(message)
    };

    const editMessage = (text, messageId) => {
        setMessages(messages.map(msg => msg.id === messageId ? {...msg, text} : msg));
        setEditedMessage(undefined);
    };

    const deleteMessage = messageId => {
        setMessages(messages.filter(msg => msg.id !== messageId));
    };

    const reactMessage = messageId => {
        setMessages(messages.map(msg => msg.id === messageId ? {...msg, isLiked: !msg.isLiked} : msg));
    };


    return (
        <div className={styles.chatContainer}>
            <ChatHeader
                authUser={authUser}
                countMessages={countMessages}
                countUsers={countUsers}
                dateLustMessage={dateLastMessage}
            />
            {Array.isArray(messages) && messages.length
                ? <MessagesList
                    messages={messages}
                    authUser={authUser}
                    reactMessage={reactMessage}
                    setEditStatusMessage={setEditStatusMessage}
                    deleteMessage={deleteMessage}
                />
                : <Spinner/>}
            <ChatFooter addMessage={addMessage} authUser={authUser}/>
            {editedMessage &&
            <EditMessage
                editedMessage={editedMessage}
                editMessage={editMessage}
            />}
        </div>
    );
};

export default Chat;
