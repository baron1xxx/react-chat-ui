import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";

const AvatarChat = ({src, status}) => {
    return (
        <div className={styles.avatarContainer}>
            <img className={styles.imgAvatar} src={src} alt="Logo"/>
            <div className={status ? styles.statusOnLine : styles.statusOfLine}></div>
        </div>
    );
};

AvatarChat.propTypes = {
    src: PropTypes.string.isRequired,
    status: PropTypes.bool.isRequired
};

export default AvatarChat;
