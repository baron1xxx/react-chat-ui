import React, {useState} from 'react';

import styles from "./styles.module.css";

const EditMessage = ({editedMessage, editMessage}) => {

    const [text, setText] = useState(editedMessage.text);

    const messageBodyChanged = data => {
        setText(data);
    };

    const handleEditMessage = (ev) => {
        ev.preventDefault();
        editMessage(text, editedMessage.id);
    };

    return (
        <div className={styles.modalOverlow} >
            <div className={styles.addMessageContainer}>
                <form className={styles.formContainer}>
                <textarea
                    className={styles.textMessage}
                    value={text}
                    placeholder="Type your message..."
                    onChange={ev => messageBodyChanged(ev.target.value)}
                >
                </textarea>
                    <button className={styles.submitButton} type='submit' disabled={!text} onClick={handleEditMessage}>
                        <div className={styles.sendIcon}><i className="fas fa-paper-plane"></i></div>
                    </button>
                </form>
            </div>
        </div>
    );
};

export default EditMessage;
