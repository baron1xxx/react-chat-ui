import React from 'react';
import Logo from "../Logo";

import styles from "./styles.module.css";

const Header = () => {
    return (
        <header className={styles.header}>
            <Logo src={'/chat-logo.png'}/>
            <div className={styles.headerChatName}>Cool React chat</div>
        </header>
    );
};

export default Header;
